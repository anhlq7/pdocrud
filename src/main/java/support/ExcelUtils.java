package support;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.cucumber.createmeta.CreateMeta;

public class ExcelUtils {
	private Workbook workbook;
	private File file;

	public ExcelUtils(String path) {
		try {
			file = new File(path);
			FileInputStream fis = new FileInputStream(file);
			workbook = new XSSFWorkbook(fis);
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Workbook getWorkbook() {
		return this.workbook;
	}

	public Cell getCell(String sheetName, int row, int column) {
		return workbook.getSheet(sheetName).getRow(row).getCell(column);
	}

	public String getStringCellValue(Cell cell) {
		if (cell == null) {
			return null;
		}
		cell.setCellType(CellType.STRING);
		return cell.getStringCellValue();
	}

	public int getLastRowNum(String sheetName) {
		Sheet sheet = workbook.getSheet(sheetName);
		if (sheet == null) {
			return -1;
		}
		return sheet.getLastRowNum();
	}

	public CellRangeAddress getCellRangeAddressByCell(String sheetname, Cell cell) {
		Sheet sheet = workbook.getSheet(sheetname);
		List<CellRangeAddress> ranges = sheet.getMergedRegions();
		for (CellRangeAddress r : ranges) {
			if (r.isInRange(cell)) {
				return r;
			}
		}
		return null;
	}

	public int getLastRangeColumn(String sheetname, Cell cell) {
		CellRangeAddress range = getCellRangeAddressByCell(sheetname, cell);
		if (range == null) {
			return -1;
		}
		return range.getLastColumn();
	}

	public void setCellValue(String sheetname, int row, int column, String value) {
		Sheet sheet = workbook.getSheet(sheetname);
		Cell cell = sheet.getRow(row).getCell(column);
		if (cell == null) {
			cell = sheet.getRow(row).createCell(column);
		}
		cell.setCellValue(value);
	}
	
	public void setCellStyle(String sheetname, int row, int column) {
		CellStyle style = workbook.createCellStyle();
		Sheet sheet = workbook.getSheet(sheetname);
	    style.setFillForegroundColor(IndexedColors.RED.getIndex());
	    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    style.setAlignment(HorizontalAlignment.CENTER);
	    style.setVerticalAlignment(VerticalAlignment.CENTER);
	    Cell cell = getCell(sheetname, row, column);
//	    sheet.getRow(row).setRowStyle(style);
	    cell.setCellStyle(style);
	}

	public void save() {
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(file);
			workbook.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public File getFile() {
		return this.file;
	}
	
	public void setHyperLink(String sheetname, int row, int column, String path) {
		Hyperlink link = workbook.getCreationHelper().createHyperlink(HyperlinkType.FILE);
		link.setAddress(new File(path).toURI().toString());
	    Cell cell = getCell(sheetname, row, column);
	    cell.setHyperlink(link);
	}
	
	public File copy(String path) {
		File nf = new File(path);
		try {
			Files.copy(file.toPath(), nf.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return nf;
	}
}
