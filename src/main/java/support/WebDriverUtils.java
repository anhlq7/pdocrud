package support;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import constant.MessageLogger;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class WebDriverUtils {

	private static WebDriver driver;
	private static Logger logger = LogManager.getLogger(WebDriverUtils.class.getName());
	private int timeOut, pollingInterval;
	private String browserName;

//	private static final String GRID_TH_CSS = "table th:nth-child(%d)";
	
	public WebDriverUtils() {
		browserName = "chrome";
		timeOut = 10;
		pollingInterval = 5;
	}
	
	public WebDriverUtils(String browserName) {
		browserName = "chrome";
		timeOut = 10;
		pollingInterval = 5;
	}
	
	public WebDriverUtils(String browserName, int timeOut, int pollingInterval) {
		this.browserName = browserName;
		this.timeOut = timeOut;
		this.pollingInterval = pollingInterval;
	}

	public void openBrowser() {
		browserName = browserName.toLowerCase().trim();
		logger.debug(MessageLogger.MSG_LOG_INFO_OPENING_BROWSER, browserName);
		if ("chrome".equals(browserName)) {
			System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if ("firefox".equals(browserName)) {
			driver = new FirefoxDriver();
		} else if ("ie".equals(browserName)) {
			driver = new InternetExplorerDriver();
		} else {
			logger.debug(MessageLogger.MSG_LOG_INFO_BROWSER_IS_NOT_SUPPORTED, browserName);
		}

		if (driver != null) {
			logger.debug(MessageLogger.MSG_LOG_PASS_BROWSER_IS_OPENED, browserName);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		} else {
			logger.debug(MessageLogger.MSG_LOG_ERROR_BROWSER_IS_NOT_OPENED, browserName);
		}

	}

	public WebElement waitForAndGetElement(final By by) {
		WebElement element = null;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeOut))
				.pollingEvery(Duration.ofSeconds(pollingInterval)).ignoring(NoSuchElementException.class);
		try {
			logger.debug(MessageLogger.MSG_LOG_INFO_WAITING_FOR_OBJX_TO_BE_PRESENT, by.toString(), timeOut);
			element = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					logger.debug(MessageLogger.MSG_LOG_INFO_ATTEMPT_TO_WAIT_FOR_OBJX, by.toString());
					return driver.findElement(by);
				}
			});
		} catch (TimeoutException e) {

		}
		if (element != null) {
			logger.debug(MessageLogger.MSG_LOG_PASS_OBJX_IS_PRESENT, by.toString(), timeOut);
		} else {
			logger.debug(MessageLogger.MSG_LOG_ERROR_CANNOT_WAIT_FOR_OBJX_TO_BE_PRESENT, by.toString(), timeOut);
		}
		return element;
	}

//	public void scrollGridHeaderToView(int index) {
//		String css = String.format(GRID_TH_CSS, index);
//		By by = By.cssSelector(css);
//		boolean isIn = verifyElementInViewport(by);
//		if(!isIn) {
//			logger.debug(MessageLogger.MSG_LOG_INFO_SCROLLING_OBJX_INTO_VIEWPORT, by.toString(), timeOut);
//			WebElement element = driver.findElement(by);
//			String script  = "arguments[0].scrollIntoView();";
//			((JavascriptExecutor) driver).executeScript(script, element);
//		}
//	}

	public boolean verifyElementInViewport(final By by) {
		WebElement element = waitForAndGetElement(by);
		boolean isIn = false;
		if (element == null) {
			return false;
		}
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeOut))
				.pollingEvery(Duration.ofSeconds(pollingInterval)).ignoring(NoSuchElementException.class);
		try {
			logger.debug(MessageLogger.MSG_LOG_INFO_VERIFYING_OBJX_IN_VIEWPORT_IN_XSECONDS, by.toString(), timeOut);
			isIn = wait.until(new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver driver) {
					logger.debug(MessageLogger.MSG_LOG_INFO_ATTEMPT_TO_VERIFY_OBJX_IN_VIEWPORT_IN_XSECONDS, by.toString());
					WebElement element = driver.findElement(by);
					String script = "var elem = arguments[0],           " 
							+ "  box = elem.getBoundingClientRect(),    "
							+ "  cx = box.left + box.width / 2,         "
							+ "  cy = box.top + box.height / 2,         "
							+ "  e = document.elementFromPoint(cx, cy); "
							+ "for (; e; e = e.parentElement) {         "
							+ "  if (e === elem)                        "
							+ "    return true;                         "
							+ "}                                        "
							+ "return false;                            ";
					return (Boolean) ((JavascriptExecutor) driver).executeScript(script,element);
				}
			});
		} catch (TimeoutException e) {
			isIn = false;
		}
		if (isIn) {
			logger.debug(MessageLogger.MSG_LOG_PASS_OBJ_X_VISIBLE_IN_VIEWPORT, by.toString());
		} else {
			logger.debug(MessageLogger.MSG_LOG_ERROR_OBJ_X_VISIBLE_IN_VIEWPORT, by.toString());
		}
		return isIn;
	}

	public void navigateTo(String url) {
		driver.get(url);
	}

	public void click(By by) {
		WebElement element = waitForAndGetElement(by);
		if (element != null) {
			try {
				logger.debug(MessageLogger.MSG_LOG_INFO_CLICKING_OBJX, by.toString());
				WebDriverWait wait = new WebDriverWait(driver, timeOut);
				boolean isClickable = (wait.until(ExpectedConditions.elementToBeClickable(element)) != null);
				if(isClickable) {
					element.click();
					logger.debug(MessageLogger.MSG_LOG_PASS_OBJX_IS_CLICKED, by.toString());
				}
				else {
					logger.debug(MessageLogger.MSG_LOG_ERROR_OBJX_IS_CLICKED, by.toString());
				}
			} catch (TimeoutException e) {
				logger.debug(MessageLogger.MSG_LOG_ERROR_OBJX_IS_CLICKED, by.toString());
			}
		}
		else {
			logger.debug(MessageLogger.MSG_LOG_ERROR_OBJX_IS_CLICKED, by.toString());
		}
	}
	
	public void setText(By by, String text) {
		WebElement element = waitForAndGetElement(by);
		logger.debug(MessageLogger.MSG_LOG_INFO_SETTING_OBJ_TXT_TO_VAL, by.toString(), text);
		if (element != null) {
			element.sendKeys(text);
			logger.debug(MessageLogger.MSG_LOG_PASS_TXT_IS_SET_ON_OBJ, text, by.toString());
		}
		else {
			logger.debug(MessageLogger.MSG_LOG_ERROR_CANNOT_SET_TXT_X_OF_OBJ, text, by.toString());
		}
	}
	
	public String getText(By by) {
		String text = null;
		WebElement element = waitForAndGetElement(by);
		logger.debug(MessageLogger.MSG_LOG_INFO_GETTING_OBJ_TXT, by.toString());
		if (element != null) {
			text = element.getText();
			logger.debug(MessageLogger.MSG_LOG_PASS_OBJ_TXT_IS,  by.toString(), text);
		}
		else {
			logger.debug(MessageLogger.MSG_LOG_ERROR_CANNOT_GET_TXT_OF_OBJ, by.toString());
		}
		return text;
	}
	
	public By getElement(String locator, String type) {
		type = type.toLowerCase();
		if ("cssselector".equals(type)) {
			return By.cssSelector(locator);
		} else if ("xpath".equals(type)) {
			return By.xpath(locator);
		}
		return null;
	}
	
	public String takeScreenshot(String path) {
		try {
			Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.simple()).takeScreenshot(driver);
			File f = new File(path);
			ImageIO.write(screenshot.getImage(), Utility.getFileExtension(f), f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;
	}
	
	public void delay(int mili) {
		try {
			Thread.sleep(mili);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeBrowser() {
		driver.close();
	}
}
