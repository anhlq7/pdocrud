package support;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.google.common.io.Files;

public class Utility {
	
	public static Properties loadProperties(String path) {
		FileInputStream fis;
		Properties p = new Properties();
		try {
			fis = new FileInputStream(path);
			p.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p;
	}
	
	public static String getCurrent(String format) {
		//"yyyy/MM/dd HH:mm:ss"
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(new Date());
	}
	
	public static String getSimpleFilename(File file) {
		return file.getName().replaceFirst("[.][^.]+$", "");
	}
	
	public static String getFileExtension(File file) {
		return Files.getFileExtension(file.getAbsolutePath());
	}
}
