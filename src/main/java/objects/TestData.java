package objects;

public class TestData {
	private TestObject object;
	private String data;

	public TestData(TestObject object, String data) {
		this.object = object;
		this.data = data;
	}

	public TestObject getObject() {
		return object;
	}

	public void setObject(TestObject object) {
		this.object = object;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TestData [object=" + object.getName() + ", data=" + data + "]";
	}

}
