package objects;

import java.util.List;

public class TestCase {
	
	private String id;
	private String name;
	private List<TestData> inputTD;
	private List<TestData> expectedResult;
	private List<TestData> actualResult;
	private List<TestData> actions;
	private boolean isPass;
	
	public TestCase(String id, String name, List<TestData> inputTD, List<TestData> expectedResult,
			List<TestData> actions) {
		super();
		this.id = id;
		this.name = name;
		this.inputTD = inputTD;
		this.expectedResult = expectedResult;
		this.actions = actions;
	}

	public List<TestData> getActualResult() {
		return actualResult;
	}

	public void setActualResult(List<TestData> actualResult) {
		this.actualResult = actualResult;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TestData> getInputTD() {
		return inputTD;
	}

	public void setInputTD(List<TestData> inputTD) {
		this.inputTD = inputTD;
	}

	public List<TestData> getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(List<TestData> expectedResult) {
		this.expectedResult = expectedResult;
	}

	public List<TestData> getActions() {
		return actions;
	}

	public void setActions(List<TestData> actions) {
		this.actions = actions;
	}

	public boolean isPass() {
		return isPass;
	}

	public void setPass(boolean isPass) {
		this.isPass = isPass;
	}

	@Override
	public String toString() {
		return "TestCase [tcId=" + id + ", tcName=" + name + ", inputTD=" + inputTD + ", expectedResult="
				+ expectedResult + ", actions=" + actions + ", isPass=" + isPass + "]";
	}
	
}
