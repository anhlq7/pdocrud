package objects;

public class TestObject {
	private String name;
	private String locator;
	private String type;
	
	public TestObject(String name, String locator, String type) {
		this.name = name;
		this.locator = locator;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocator() {
		return locator;
	}

	public void setLocator(String locator) {
		this.locator = locator;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "TestObject [name=" + name + ", locator=" + locator + "]";
	}
}
