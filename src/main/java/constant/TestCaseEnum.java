package constant;

public class TestCaseEnum {

	public enum DocumentHeader {
		INPUT_DATA_HEADER(7, 4);

		int firstRow, firstCol;

		private DocumentHeader(int firstRow, int firstCol) {
			this.firstRow = firstRow;
			this.firstCol = firstCol;
		}

		public int getFirstRow() {
			return firstRow;
		}

		public int getFirstColumn() {
			return firstCol;
		}
	}

	public enum DocumentBody {
		ID(0), NAME(1), STATUS(2), DURATION(3);

		int index;

		private DocumentBody(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}
	}

	public enum TestCaseSummary {
		PASS(1, 1),
		FAIL(2, 1),
		TOTAL_TEST_CASE(1, 5),
		PASS_PERCENT(2, 5),
		EXECUTION_DATE(4,1),
		TOTAL_EXECUTION_TIME(4,5);
		private int row;
		private int column;
		
		private TestCaseSummary(int row, int column) {
			this.row = row;
			this.column = column;
		}
		
		public int getRow() {
			return this.row;
		}
		
		public int getColumn() {
			return this.column;
		}
 		
	}
}
