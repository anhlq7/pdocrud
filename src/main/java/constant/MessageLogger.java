package constant;

public class MessageLogger {
	
	public static String MSG_LOG_INFO_OPENING_BROWSER = "Opening browser {}.";
	public static String MSG_LOG_PASS_BROWSER_IS_OPENED = "Browser {} is opened.";
	public static String MSG_LOG_INFO_BROWSER_IS_NOT_SUPPORTED = "Browser {} is not supported.";
	public static String MSG_LOG_ERROR_BROWSER_IS_NOT_OPENED = "Browser {} is not opened.";
	
	public static String MSG_LOG_INFO_WAITING_FOR_OBJX_TO_BE_PRESENT = "Waiting for [{}] to be present in {} seconds";
	public static String MSG_LOG_PASS_OBJX_IS_PRESENT = "[{}] is present";
	public static String MSG_LOG_ERROR_CANNOT_WAIT_FOR_OBJX_TO_BE_PRESENT = "Unable to wait for [{}] to be present after {} seconds";
	
	public static String MSG_LOG_INFO_ATTEMPT_TO_WAIT_FOR_OBJX = "Attempting to wait for [{}]";
	
	public static String MSG_LOG_INFO_SCROLLING_OBJX_INTO_VIEWPORT = "Scrolling [{}] into the viewport.";
	
	public static String MSG_LOG_PASS_OBJ_X_VISIBLE_IN_VIEWPORT = "[{}] is present and visible in viewport.";
	public static String MSG_LOG_ERROR_OBJ_X_VISIBLE_IN_VIEWPORT = "[{}] is present but is not visible in viewport.";
	
	public static String MSG_LOG_INFO_VERIFYING_OBJX_IN_VIEWPORT_IN_XSECONDS = "Verifying if [{}] is present and visible in viewport in {} seconds.";
	public static String MSG_LOG_INFO_ATTEMPT_TO_VERIFY_OBJX_IN_VIEWPORT_IN_XSECONDS = "Attempting to verify if [{}] is present and visible in viewport.";
	
	public static String MSG_LOG_INFO_CLICKING_OBJX = "Clicking [{}] element.";
	public static String MSG_LOG_PASS_OBJX_IS_CLICKED = "[{}] is clicked";
	public static String MSG_LOG_ERROR_OBJX_IS_CLICKED = "[{}] cannot be clicked";
	
	public static String MSG_LOG_INFO_SETTING_OBJ_TXT_TO_VAL = "Setting [{}] element text to {}";
	public static String MSG_LOG_PASS_TXT_IS_SET_ON_OBJ = "Text {} is set on element [{}]";
	public static String MSG_LOG_ERROR_CANNOT_SET_TXT_X_OF_OBJ = "Unable to set text {} of element [{}]";
	
	public static String MSG_LOG_INFO_GETTING_OBJ_TXT = "Getting text of element [{}]";
	public static String MSG_LOG_PASS_OBJ_TXT_IS = "Text of element [{}] is: {}";
	public static String MSG_LOG_ERROR_CANNOT_GET_TXT_OF_OBJ = "Unable to get text of element [{}]";
	
}
