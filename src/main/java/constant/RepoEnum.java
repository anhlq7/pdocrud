package constant;

public class RepoEnum {

	// define header row
	public enum Header {
		ROW(0);

		int row;

		private Header(int row) {
			this.row = row;
		}

		public int getRow() {
			return row;
		}
	}

	public enum Body {
		NAME(0), LOCATOR(1), TYPE(2);

		int index;

		private Body(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}
	}
}
