package app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import constant.RepoEnum;
import objects.TestObject;
import support.ExcelUtils;
import support.Utility;

public class ObjectRepoLoader {
	private Properties p;
	private ExcelUtils util;
	private List<TestObject> elements;
	private String sheetname;
	private HashMap<String, TestObject> hm;
	public ObjectRepoLoader(String sheetname) {
		elements = new ArrayList<TestObject>();
		p = Utility.loadProperties("config.properties");
		util = new ExcelUtils(p.getProperty("objectRepo"));
		this.sheetname = sheetname;
		hm = new HashMap<String, TestObject>();
	}
	
	public void setSheetName(String sheetname) {
		this.sheetname = sheetname;
	}
	
	public void loadElements() {
		int firstRow = RepoEnum.Header.ROW.getRow() + 1;
		int lastRow = util.getLastRowNum(sheetname);
		int idxName = RepoEnum.Body.NAME.getIndex();
		int idxLocator = RepoEnum.Body.LOCATOR.getIndex();
		int idxType = RepoEnum.Body.TYPE.getIndex();
		String name, locator, type = null;
		TestObject e = null;
		for(int i = firstRow; i <= lastRow; i++) {
			name = util.getStringCellValue(util.getCell(sheetname, i, idxName));
			locator = util.getStringCellValue(util.getCell(sheetname, i, idxLocator));
			type = util.getStringCellValue(util.getCell(sheetname, i, idxType));
			e = new TestObject(name, locator, type);
			elements.add(e);
			hm.put(e.getName(), e);
		}
	}
	
	public TestObject getElementByName(String name) {
		return hm.get(name);
	}
	
	public List<TestObject> getElements() {
		return this.elements;
	}
}
