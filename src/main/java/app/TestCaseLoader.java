package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;
import constant.TestCaseEnum;
import objects.TestCase;
import objects.TestData;
import support.ExcelUtils;
import support.Utility;

public class TestCaseLoader {

	private ExcelUtils util;
	private String sheetname;
	private ObjectRepoLoader objectRepoLoader;
	private List<TestCase> tcs;

	public TestCaseLoader(ExcelUtils util, String sheetname) {
		this.util = util;
		this.sheetname = sheetname;
		// load elements
		objectRepoLoader = new ObjectRepoLoader(sheetname);
		objectRepoLoader.loadElements();
		tcs = new ArrayList<TestCase>();
	}

	private List<TestData> getTestData(CellRangeAddress range, int row) {
		List<TestData> lsData = new ArrayList<TestData>();
		int idxInputDataRow = TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow();
		row += (idxInputDataRow + 2);
		String elementName, elementData = null;
		TestData data;
		for (int i = range.getFirstColumn(); i <= range.getLastColumn(); i++) {
			elementName = util.getStringCellValue(util.getCell(sheetname, idxInputDataRow + 1, i));
			elementData = util.getStringCellValue(util.getCell(sheetname, row, i));
			data = new TestData(objectRepoLoader.getElementByName(elementName), elementData);
			lsData.add(data);
		}
		return lsData;
	}

	// Input Data
	private Cell getFirstInputDataCell() {
		int frow = TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow();
		int fcolumn = TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstColumn();
		return util.getCell(sheetname, frow, fcolumn);
	}

	private CellRangeAddress getInputDataRange() {
		return util.getCellRangeAddressByCell(sheetname, getFirstInputDataCell());
	}

	private List<TestData> getInputDataByRow(int row) {
		return getTestData(getInputDataRange(), row);
	}

	private int getLastInputDataColumn() {
		return util.getLastRangeColumn(sheetname, getFirstInputDataCell());
	}

	// Expected Result
	private Cell getFirstEResultCell() {
		int fcolumn = getLastInputDataColumn() + 1;
		int frow = TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow();
		return util.getCell(sheetname, frow, fcolumn);
	}

	private CellRangeAddress getEResultRange() {
		return util.getCellRangeAddressByCell(sheetname, getFirstEResultCell());
	}

	// get expected result by row
	public List<TestData> getEResultByRow(int row) {
		return getTestData(getEResultRange(), row);
	}

	private int getLastEResultColumn() {
		return util.getLastRangeColumn(sheetname, getFirstEResultCell());
	}

	// Actual Result
	
	public Cell getFirstAResultCell() {
		int fcolumn = getLastEResultColumn() + 1;
		int frow = TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow();
		return util.getCell(sheetname, frow, fcolumn);
	}

	private CellRangeAddress getAResultRange() {
		return util.getCellRangeAddressByCell(sheetname, getFirstAResultCell());
	}

	public List<TestData> getAResultByRow(int row) {
		return getTestData(getAResultRange(), row);
	}
	
	public void setAResultValue(int row, int column, String value) {
		row = row + TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow() + 2;
		util.setCellValue(sheetname, row, column, value);
		util.save();
	}

	private int getLastAResultColumn() {
		return util.getLastRangeColumn(sheetname, getFirstAResultCell());
	}

	// Action
	private Cell getFirstActionCell() {
		int fcolumn = getLastAResultColumn() + 1;
		int frow = TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow();
		return util.getCell(sheetname, frow, fcolumn);
	}

	private CellRangeAddress getActionRange() {
		return util.getCellRangeAddressByCell(sheetname, getFirstActionCell());
	}

	// get expected result by row
	private List<TestData> getActionByRow(int row) {
		return getTestData(getActionRange(), row);
	}

	public void loadTestCase() {
		int lrow = util.getLastRowNum(sheetname) - TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow() - 2;
		String id;
		String name;
		List<TestData> inputData;
		List<TestData> expectResult;
		List<TestData> actions;
		for (int i = 0; i <= lrow; i++) {
			id = util.getStringCellValue(util.getCell(sheetname, 
					i + TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow() + 2, 
					TestCaseEnum.DocumentBody.ID.getIndex()));
			name = util.getStringCellValue(util.getCell(sheetname, 
					i + TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow() + 2, 
					TestCaseEnum.DocumentBody.NAME.getIndex()));
			inputData = getInputDataByRow(i);
			expectResult = getEResultByRow(i);
			actions = getActionByRow(i);
			tcs.add(new TestCase(id, name, inputData, expectResult, actions));
		}
	}
	
	public ExcelUtils getExcel() {
		return this.util;
	}
	
	public List<TestCase> getTestCases() {
		return this.tcs;
	}

}
