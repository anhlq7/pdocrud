package test;

import org.testng.annotations.Test;

import app.TestCaseLoader;
import constant.TestCaseEnum;
import objects.TestCase;
import objects.TestData;
import objects.TestObject;
import support.ExcelUtils;
import support.Utility;
import support.WebDriverUtils;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.poi.hpsf.Array;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;

public class AddTest {

	private static int currentRow = 0;
	private String sheetname;
	private TestCaseLoader loader;
	private List<TestCase> tcs;
	private WebDriverUtils driver;
	private Properties p;
	private static int passNum = 0;
	private static int failNum = 0;
	private static int caseNum = 0;
	private static int exeTime = 0;
	private ExcelUtils testCase;
	private ExcelUtils fReport;
	private String screenshot;

	@BeforeTest
	public void beforeTest() {
		sheetname = this.getClass().getSimpleName();
		p = Utility.loadProperties("config.properties");
		testCase = new ExcelUtils(p.getProperty("testCase"));
		fReport = new ExcelUtils(testCase.copy(p.getProperty("report")
				.concat(Utility.getSimpleFilename(testCase.getFile())).concat(Utility.getCurrent("yyyyMMddHHmmss"))
				.concat(".").concat(Utility.getFileExtension(testCase.getFile()))).getAbsolutePath());
		loader = new TestCaseLoader(fReport, sheetname);
		loader.loadTestCase();
		tcs = loader.getTestCases();
		String browser = p.getProperty("browser");
		int timeOut = Integer.parseInt(p.getProperty("defaultTimeOut"));
		int pollingInterval = Integer.parseInt(p.getProperty("defaultPollingInterval"));
		driver = new WebDriverUtils(browser, timeOut, pollingInterval);
		driver.openBrowser();
		driver.navigateTo(p.getProperty("baseUrl"));
	}

	@DataProvider
	public Object[][] dp() {
		int column = 6;
		int row = tcs.size();
		Object[][] dp = new Object[row][column];
		for (int i = 0; i < row; i++) {
			dp[i][0] = tcs.get(i).getId(); // id
			dp[i][1] = tcs.get(i).getName(); // name
			dp[i][2] = tcs.get(i).getInputTD(); // test data
			dp[i][3] = tcs.get(i).getExpectedResult(); // expected result
			dp[i][4] = tcs.get(i).getActions(); // action
		}
		return dp;
	}

	@Test(dataProvider = "dp")
	public void test(Object... o) {
		caseNum++;
		List<TestData> lsInputData = (List<TestData>) o[2];
		setData(lsInputData);
		List<TestData> lsAction = (List<TestData>) o[4];
		action(lsAction);
		List<TestData> lsExp = (List<TestData>) o[3];
		setActualResult(lsExp);
		List<TestData> lsAct = loader.getAResultByRow(currentRow);
		Assert.assertTrue(validate(lsExp, lsAct));
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {
		currentRow++;
		long duration = result.getEndMillis() - result.getStartMillis();
		exeTime += duration;
		try {
			int indStatus = TestCaseEnum.DocumentBody.STATUS.getIndex();
			int indDuration = TestCaseEnum.DocumentBody.DURATION.getIndex();
			int frow = TestCaseEnum.DocumentHeader.INPUT_DATA_HEADER.getFirstRow() + currentRow + 1;
			if (result.getStatus() == ITestResult.SUCCESS) {
				fReport.setCellValue(sheetname, frow, indStatus, "Pass");
				passNum++;
			}

			else if (result.getStatus() == ITestResult.FAILURE) {
				fReport.setCellValue(sheetname, frow, indStatus, "Fail");
				fReport.setCellStyle(sheetname, frow, indStatus);
				screenshot = driver.takeScreenshot(p.getProperty("screenshot")
						.concat(Utility.getCurrent("yyyyMMddHHmmss"))
						.concat(".")
						.concat("png"));
				fReport.setHyperLink(sheetname, frow, indStatus, screenshot);
				failNum++;
			}

			else if (result.getStatus() == ITestResult.SKIP) {
				loader.getExcel().setCellValue(sheetname, frow, indStatus, "Skip");
			}
			fReport.setCellValue(sheetname, frow, indDuration, String.valueOf(duration).concat(" ms"));
			fReport.save();
		} catch (Exception e) {
			e.printStackTrace();
		}
		driver.navigateTo(p.getProperty("baseUrl"));
	}

	private void setData(List<TestData> ls) {
		for (int i = 0; i < ls.size(); i++) {
			TestData td = ls.get(i);
			TestObject element = td.getObject();
			By by = driver.getElement(element.getLocator(), element.getType());
			driver.click(by);
			driver.setText(by, td.getData());
		}
	}

	private void action(List<TestData> ls) {
		for (int i = 0; i < ls.size(); i++) {
			TestData action = ls.get(i);
			String data = action.getData();
			if (!data.isEmpty()) {
				TestObject element = action.getObject();
				driver.click(driver.getElement(element.getLocator(), element.getType()));
				break;
			}
		}
	}

	private void setActualResult(List<TestData> ls) {
		int column = loader.getFirstAResultCell().getAddress().getColumn();
		for (int i = 0; i < ls.size(); i++) {
			TestData ar = ls.get(i);
			TestObject element = ar.getObject();
			String text = driver.getText(driver.getElement(element.getLocator(), element.getType()));
			loader.setAResultValue(currentRow, column, text);
			column++;
		}
	}

	private boolean validate(List<TestData> exp, List<TestData> act) {
		if (exp.size() != act.size()) {
			return false;
		}
		for (int i = 0; i < act.size(); i++) {
			if (!exp.get(i).getData().equals(act.get(i).getData())) {
				return false;
			}
		}
		return true;
	}

	@AfterTest
	public void afterTest() {
		driver.closeBrowser();
		report();
	}

	private void report() {

		fReport.setCellValue(sheetname, TestCaseEnum.TestCaseSummary.PASS.getRow(),
				TestCaseEnum.TestCaseSummary.PASS.getColumn(), String.valueOf(passNum));

		fReport.setCellValue(sheetname, TestCaseEnum.TestCaseSummary.FAIL.getRow(),
				TestCaseEnum.TestCaseSummary.FAIL.getColumn(), String.valueOf(failNum));

		fReport.setCellValue(sheetname, TestCaseEnum.TestCaseSummary.TOTAL_TEST_CASE.getRow(),
				TestCaseEnum.TestCaseSummary.TOTAL_TEST_CASE.getColumn(), String.valueOf(caseNum));

		fReport.setCellValue(sheetname, TestCaseEnum.TestCaseSummary.PASS_PERCENT.getRow(),
				TestCaseEnum.TestCaseSummary.PASS_PERCENT.getColumn(),
				String.format("%.2f", (double) passNum / caseNum * 100).concat("%"));

		fReport.setCellValue(sheetname, TestCaseEnum.TestCaseSummary.EXECUTION_DATE.getRow(),
				TestCaseEnum.TestCaseSummary.EXECUTION_DATE.getColumn(), Utility.getCurrent("yyyy-MM-dd"));

		fReport.setCellValue(sheetname, TestCaseEnum.TestCaseSummary.TOTAL_EXECUTION_TIME.getRow(),
				TestCaseEnum.TestCaseSummary.TOTAL_EXECUTION_TIME.getColumn(), String.valueOf(exeTime).concat(" ms"));

		fReport.save();
	}

}
